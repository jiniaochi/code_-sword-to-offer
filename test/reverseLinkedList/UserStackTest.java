package reverseLinkedList;

import org.junit.Before;
import org.junit.Test;
import reverseLinkedList.dto.LinedListDto;
import reverseLinkedList.dto.LinkedItem;
import reverseLinkedList.my.UseStack;

public class UserStackTest {

    LinedListDto listDto = null;

    @Before
    public void initData(){
        listDto = new LinedListDto();
        listDto.addItem(new LinkedItem("A",null));
        listDto.addItem(new LinkedItem("B",null));
        listDto.addItem(new LinkedItem("C",null));
        listDto.addItem(new LinkedItem("D",null));
        listDto.addItem(new LinkedItem("E",null));
        listDto.addItem(new LinkedItem("F",null));
        listDto.addItem(new LinkedItem("G",null));
        listDto.addItem(new LinkedItem("H",null));
        listDto.addItem(new LinkedItem("I",null));
    }

    @Test
    public void testUserStackTest(){
        UseStack.printReverseList(this.listDto);
    }
}
