package stack2queue;

import org.junit.Test;

public class TestStackQueue {

    @Test
    public void test1(){
        StackQueue stackQueue = new StackQueue();
        stackQueue.addElement("A");
        stackQueue.addElement("B");
        stackQueue.addElement("C");
        stackQueue.addElement("D");

        System.out.println(stackQueue.removeElement());
        System.out.println(stackQueue.removeElement());

        stackQueue.addElement("E");
        System.out.println(stackQueue.removeElement());
        System.out.println(stackQueue.removeElement());
        System.out.println(stackQueue.removeElement());

    }
}
