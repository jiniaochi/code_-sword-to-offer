package twoDimensionSearch;

import org.junit.Assert;
import org.junit.Test;

public class TestCorrectSolution2 {

    @Test
    public void testCorrectSolution2(){
        int[][] arr = new int[][]{{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
        CorrectSolution2 correctSolution2 = new CorrectSolution2(arr, 10);
        boolean iterationRes = correctSolution2.findByIteration();
        Assert.assertTrue("测试不通过",iterationRes);
    }

    @Test
    public void testCorrectSolution2_2(){
        int[][] arr = new int[][]{{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
        CorrectSolution2 correctSolution2 = new CorrectSolution2(arr, 3);
        boolean iterationRes = correctSolution2.findByIteration();
        Assert.assertFalse("测试不通过",iterationRes);
    }
}
