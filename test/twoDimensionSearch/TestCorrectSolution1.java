package twoDimensionSearch;

import org.junit.Assert;
import org.junit.Test;

public class TestCorrectSolution1 {

    @Test
    public void testCorrectSolution1(){
        int[][] arr = new int[][]{{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
        CorrectSolution1 solution1 = new CorrectSolution1(arr, 7);
        boolean recursionRes = solution1.searchNumByRecursion();
        Assert.assertTrue("����ʧ��",recursionRes);
    }

    @Test
    public void testCorrectSolution1_2(){
        int[][] arr = new int[][]{{1,2,8,9},{2,4,9,12},{4,7,10,13},{6,8,11,15}};
        CorrectSolution1 solution1 = new CorrectSolution1(arr, 13);
        boolean recursionRes = solution1.searchNumByRecursion();
        Assert.assertTrue("����ʧ��",recursionRes);
    }
}
