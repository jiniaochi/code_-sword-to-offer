package singleton;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author jinxiaozhi
 * @date 2021年11月16日
 * @effect 单例模式解决方案1--私有化构造函数--测试类
 * @disadvantage 缺点：不适用于多线程，多线程可能会同时调用同一个方法。
 */
public class Singleton1Test {

    @Test
    public void testSingleThread() {
        Singleton1 instance = Singleton1.getInstance();
        System.out.println(instance);
        System.out.println(Singleton1.getInstance());
    }

    @Test
    public void testMutiThread01() {//使用 CountDownLatch 作为信号量
        CountDownLatch countDownLatch = new CountDownLatch(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    countDownLatch.await();
                    System.out.println(Singleton1.getInstance());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            countDownLatch.countDown();
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

    @Test
    public void testMutiThread02() {//使用 CyclicBarrier 作为信号量
        CyclicBarrier barrier = new CyclicBarrier(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    barrier.await();
                    System.out.println(Singleton1.getInstance());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

}
