package singleton;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单例模式
 * 加上同步锁--加锁双if法，优化单if方式，外层再加一层非null判断且不用加锁
 * 缺点： 代码较为复杂
 */
public class Singleton3Test {

    @Test
    public void testSingleThread() {
        Singleton3 instance = Singleton3.getInstance();
        System.out.println(instance);
        System.out.println(Singleton3.getInstance());
    }

    @Test
    public void testMutiThread01() {//使用 CountDownLatch 作为信号量
        CountDownLatch countDownLatch = new CountDownLatch(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    countDownLatch.await();
                    System.out.println(Singleton3.getInstance());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            countDownLatch.countDown();
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

    @Test
    public void testMutiThread02() {//使用 CyclicBarrier 作为信号量
        CyclicBarrier barrier = new CyclicBarrier(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    barrier.await();
                    System.out.println(Singleton3.getInstance());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

}
