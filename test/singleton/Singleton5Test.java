package singleton;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单例模式: 推荐
 * 静态成员变量，直接调用构造函数
 * 优点：不用加锁也同样适用于多线程
 * 缺点： 类加载就开始创建单例，可能浪费内存
 */
public class Singleton5Test {

    @Test
    public void testSingleThread() {
        Singleton5 instance = Singleton5.getInstance();
        System.out.println(instance);
        System.out.println(Singleton5.getInstance());
    }

    @Test
    public void testMutiThread01() {//使用 CountDownLatch 作为信号量
        CountDownLatch countDownLatch = new CountDownLatch(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    countDownLatch.await();
                    System.out.println(Singleton5.getInstance());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            countDownLatch.countDown();
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

    @Test
    public void testMutiThread02() {//使用 CyclicBarrier 作为信号量
        CyclicBarrier barrier = new CyclicBarrier(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    barrier.await();
                    System.out.println(Singleton5.getInstance());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

}
