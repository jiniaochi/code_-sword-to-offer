package singleton;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单例模式: 推荐
 * 枚举类模式
 * 优点：线程安全，调用效率高，可以天然的防止反射和反序列化调用
 * 缺点： 不能延时加载
 */
public class Singleton6Test {

    @Test
    public void testSingleThread() {
        System.out.println(Singleton6.INSTANCE);
        System.out.println(Singleton6.INSTANCE);
    }

    @Test
    public void testMutiThread01() {//使用 CountDownLatch 作为信号量
        CountDownLatch countDownLatch = new CountDownLatch(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    countDownLatch.await();
                    System.out.println(Singleton6.INSTANCE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            countDownLatch.countDown();
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

    @Test
    public void testMutiThread02() {//使用 CyclicBarrier 作为信号量
        CyclicBarrier barrier = new CyclicBarrier(1);//先拦住所有线程，然后让其同时执行
        ExecutorService threadPool = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            threadPool.execute(()-> {
                try {
                    barrier.await();
                    System.out.println(Singleton6.INSTANCE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }


        try {
            Thread.sleep(3000);//主线程沉睡，防止单元测试主线程过早结束
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            threadPool.shutdown();
        }

    }

}
