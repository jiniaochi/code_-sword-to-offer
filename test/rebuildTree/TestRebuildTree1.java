package rebuildTree;

import org.junit.Test;
import rebuildTree.Q1.RebuildTree1;

/**
 * 根据先序遍历和中序遍历确定一棵树
 */
public class TestRebuildTree1 {

    @Test
    public void test01(){
        String[] firstArr = new String[]{"1","2","4","7","3","5","6","8"};
        String[] middleArr = new String[]{"4","7","2","1","5","3","8","6"};
        RebuildTree1 tree1 = new RebuildTree1();
        tree1.rebuild(firstArr,middleArr);
    }
}
