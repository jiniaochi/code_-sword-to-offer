package singleton;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 单例模式: 静态成员变量-推荐
 * 静态成员变量，直接调用构造函数
 * 优点：不用加锁也同样适用于多线程
 * 缺点： 类加载就开始创建单例，可能浪费内存
 */
public class Singleton4 {

    private static Singleton4 instance = new Singleton4();
    private String randNum = null;

    public static Singleton4 getInstance(){
        return instance;
    }

    @Override
    public String toString() {
        return "[ randNum:" + this.randNum + "]";
    }

    private Singleton4(){
        this.randNum = new Random().nextInt(100) + new Date().toString();
    }

}
