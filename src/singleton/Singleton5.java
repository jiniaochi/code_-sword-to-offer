package singleton;

import java.util.Date;
import java.util.Random;

/**
 * 单例模式: 静态内部类-推荐
 * 静态内部类实现模式
 * 优点：线程安全，调用效率高，可以延时加载（因为Java对内部类的加载机制时按需加载）
 * 缺点： 几乎完美
 */
public class Singleton5 {

    private String randNum = null;

    private static class InnnerClass{
        private static Singleton5 instance = new Singleton5();
    }

    public static Singleton5 getInstance(){
        System.out.println("开始加载类");
        return InnnerClass.instance;
    }

    @Override
    public String toString() {
        return "[ randNum:" + this.randNum + "]";
    }

    private Singleton5(){
        System.out.println("单例对象创建");
        this.randNum = new Random().nextInt(100) + new Date().toString();
    }

}
