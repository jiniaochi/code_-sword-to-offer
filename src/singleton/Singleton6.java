package singleton;

import java.util.Date;
import java.util.Random;

/**
 * 单例模式: 推荐
 * 枚举类模式
 * 优点：线程安全，调用效率高，可以天然的防止反射和反序列化调用
 * 缺点： 不能延时加载
 */
public enum Singleton6 {

    INSTANCE;
    private String randNum = null;

    @Override
    public String toString() {
        return "[ randNum:" + this.randNum + "]";
    }

    private Singleton6(){
        System.out.println("单例对象创建");
        this.randNum = new Random().nextInt(100) + new Date().toString();
    }

}
