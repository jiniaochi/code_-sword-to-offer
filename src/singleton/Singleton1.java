package singleton;

import org.junit.Test;

import java.util.Date;
import java.util.Random;

/**
 * @author jinxiaozhi
 * @date  2021年11月16日
 * @effect 单例模式解决方案1--私有化构造函数，书写简单
 * @disadvantage 缺点：不适用于多线程，多线程可能会同时调用同一个方法。
 */
public class Singleton1 {

	 private static Singleton1 instance = null;
	 private String randNum = null;//对象唯一值

	 public static Singleton1 getInstance(){
		 if(instance==null){
			 instance = new Singleton1();
		 }
		 return instance;
	 }

	@Override
	public String toString() {
		return "[ randNum:" + this.randNum + "]";
	}

	private Singleton1(){
		 this.randNum = new Random().nextInt(100) + new Date().toString();
	}

}
