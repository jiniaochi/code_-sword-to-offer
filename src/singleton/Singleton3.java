package singleton;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 单例模式 双重锁判断机制
 * 加上同步锁--加锁双if法，优化单if方式，外层再加一层非null判断且不用加锁
 * Double CheckLock实现单例：DCL也就是双重锁判断机制（由于JVM底层模型原因，偶尔会出问题，不建议使用）
 * 缺点： 代码较为复杂
 */
public class Singleton3 {

    private static Singleton3 instance = null;
    private static ReentrantLock lock = new ReentrantLock();
    private String randNum = null;

    public static Singleton3 getInstance(){
        if(instance == null){
            lock.lock();//加锁
            if(instance == null){
                instance = new Singleton3();
            }
            lock.unlock();//解锁
        }
        return instance;
    }

    @Override
    public String toString() {
        return "[ randNum:" + this.randNum + "]";
    }

    private Singleton3(){
        this.randNum = new Random().nextInt(100) + new Date().toString();
    }

}
