package singleton;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 单例模式
 * 加上同步锁--加锁单if法，保证多线程运行
 * 缺点：性能非最优，当instance=null时依然要上锁，造成性能浪费
 */
public class Singleton2 {

    private static Singleton2 instance = null;
    private static ReentrantLock lock = new ReentrantLock();
    private String randNum = null;

    public static Singleton2 getInstance(){
        lock.lock();//加锁
        if(instance == null){
            instance = new Singleton2();
        }
        lock.unlock();//解锁
        return instance;
    }

    @Override
    public String toString() {
        return "[ randNum:" + this.randNum + "]";
    }

    private Singleton2(){
        this.randNum = new Random().nextInt(100) + new Date().toString();
    }

}
