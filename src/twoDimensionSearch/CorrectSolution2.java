package twoDimensionSearch;

/**
 * 从右上或左下角开始比较，每次比较减少一列或一行;
 * 迭代写法
 */
public class CorrectSolution2 {

    private int[][] matrix;
    private int num;

    public CorrectSolution2(int[][] matrix,int num){
        this.matrix = matrix;
        this.num = num;
    }

    //迭代写法(从左下角减少范围)
    public boolean findByIteration(){
        if(this.matrix.length==0){
            return false;
        }
        int row = this.matrix.length - 1, column = 0;
        while (row>=0 && column<this.matrix[0].length){
//            if(row<0||column>=this.matrix[0].length){
//                break;
//            }

            if(this.matrix[row][column]==this.num){
                return true;
            }else if(this.matrix[row][column]>this.num){//减少一行（向上平移一格）
                row--;
            }else if(this.matrix[row][column]<this.num){//减少一列（向右平移一个）
                column++;
            }
        }

        return false;
    }



}
