package twoDimensionSearch;

/**
 * 从右上或左下角开始比较，每次比较减少一列或一行;
 * 递归写法
 */
public class CorrectSolution1 {

    private int[][] matrix;
    private int num;

    public CorrectSolution1(int[][] matrix,int num){
        this.matrix = matrix;
        this.num = num;
    }

    //先开始递归写法(从右上角开始缩小范围)
    public boolean searchNumByRecursion(){
        if(this.matrix.length==0){
            return false;
        }
        return this.findByRecursion(0,this.matrix[0].length-1);
    }

    private boolean findByRecursion(int row,int column){
        if(row>=this.matrix.length||column<0){
            return false;
        }

        if(this.matrix[row][column]==this.num){
            return true;
        }else if(this.matrix[row][column]>this.num){//减少一列(左移一列)
            return findByRecursion(row,column-1);
        }else if(this.matrix[row][column]<this.num){//减少一行(下移一行)
            return findByRecursion(row+1,column);
        }else{//这一句不会执行
            return false;
        }

    }
}
