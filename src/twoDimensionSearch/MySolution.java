package twoDimensionSearch;

/**
 * 错误点： 不能从左上角或右下角开始比较，因为这样不能缩小范围（减少行或列）
 * 应该从右上或左下开始比较
 */
public class MySolution {

    private int[][] matrix;
    private int num;

    public MySolution(int[][] matrix,int num){
        this.matrix = matrix;
        this.num = num;
    }

    /*因为该矩阵的从左到右，从上到下均为升序排列，所以可以先比较对角线上的元素（不一定是正方形），然后再上下/左右平移比较元素*/
    //注意:如果每个一维数组的长度不一致，那么就不可能实现从上到下升序排列
    public boolean searchNum(){
        int maxRows = this.matrix.length;
        if(maxRows==0){
            return false;
        }
        int maxColumn = this.matrix[0].length;
        int rowIndex = 0, columnIndex = 0;
        if(this.matrix[rowIndex][columnIndex]>num){
            return false;
        }
        //1.在对角线和对角线旁边的位置
        while (true){
            //正好匹配
            if(this.matrix[rowIndex][columnIndex] == this.num){
                return true;
            }
            if(rowIndex==maxRows||columnIndex==maxColumn){//一方到达最大范围跳出
                break;
            }
            if(this.matrix[rowIndex][columnIndex]<this.num){
                //向对角线平移
                rowIndex++;
                columnIndex++;
            }else{//大于的情况
                //判断atrix[rowIndex][columnIndex] 的上面一个和左边一个，二者都不相等则为false
                if(this.matrix[rowIndex][columnIndex - 1] == this.num){
                    return true;
                }else if(this.matrix[rowIndex - 1][columnIndex] == this.num){
                    return true;
                }else{
                    return false;
                }
            }
        }

        //2.不在对角线和对角线旁边的位置
        if(rowIndex == maxRows){

        }else if(columnIndex == maxColumn){

        }

        return false;
    }

    public static void main(String[] args) {
        float a= 13f,b=2f;
        System.out.println(a/b);
    }
}
