package countBackwardsNode14.dto;

/**
 * 单向链表
 */
public class MyLinkedList {

    public class LinkedItem {
        public String value = "";
        public  LinkedItem nextNode = null;
        public LinkedItem() { }
        public LinkedItem(String value){
            this.value = value;
        }
    }

    public LinkedItem root = null;//头节点
    public LinkedItem endNode = null;//指向尾节点，方便添加节点
    public int length = 0;

    public MyLinkedList(){
        this.root = new LinkedItem();
        this.root.value = "root";
        this.endNode = root;
    }

    //尾部追加节点
    public void addEndNode(LinkedItem node){
        if (node!=null){
            this.endNode.nextNode = node;
            this.endNode = node;
            this.length++;
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        LinkedItem node = this.root.nextNode;
        while (node!=null){
            builder.append("-->["+node.value+"]");
            node = node.nextNode;
        }

        return builder.toString();
    }
}


