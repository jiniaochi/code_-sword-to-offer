package countBackwardsNode14;

import countBackwardsNode14.dto.MyLinkedList;
import countBackwardsNode14.dto.MyLinkedList.LinkedItem;

public class DoublePointer {

    public LinkedItem findBackNode(int k, MyLinkedList list){
        if (k>0 && k<list.length){

            //1.将p1指向第k个节点
            LinkedItem p1 = list.root;
            for (int i = 0; i < k; i++) {
                p1 = p1.nextNode;
            }

            //2.将p2指向第1个节点
            LinkedItem p2 = list.root.nextNode;

            //3.p1,p2同时向链表尾部移动，直到p1指向尾节点，则p2指向倒数第k个节点
            while (p1.nextNode!=null){
                p1 = p1.nextNode;
                p2 = p2.nextNode;
            }

            return p2;
        }else {
            return list.new LinkedItem("Null");
        }

    }

    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        linkedList.addEndNode( linkedList.new LinkedItem("A") );
        linkedList.addEndNode( linkedList.new LinkedItem("B") );
        linkedList.addEndNode( linkedList.new LinkedItem("C") );
        linkedList.addEndNode( linkedList.new LinkedItem("D") );
        linkedList.addEndNode( linkedList.new LinkedItem("E") );
        linkedList.addEndNode( linkedList.new LinkedItem("F") );
        System.out.println(linkedList);

        DoublePointer doublePointer = new DoublePointer();
        System.out.println(doublePointer.findBackNode(1,linkedList).value);
        System.out.println(doublePointer.findBackNode(3,linkedList).value);
        System.out.println(doublePointer.findBackNode(0,linkedList).value);
        System.out.println(doublePointer.findBackNode(5,linkedList).value);
        System.out.println(doublePointer.findBackNode(10,linkedList).value);
    }
}
