package countBackwardsNode14;

import countBackwardsNode14.dto.MyLinkedList;
import countBackwardsNode14.dto.MyLinkedList.LinkedItem;

import java.util.Stack;

public class StackWay {

    public LinkedItem findBackNode(int k, MyLinkedList list){
        LinkedItem outPutNode = list.new LinkedItem("NUll");
        if (k>0 && k<list.length){
            Stack<LinkedItem> stack = new Stack<>();
            LinkedItem node = list.root;
            while (node.nextNode!=null){
                node = node.nextNode;
                stack.push(node);
            }



            for (int i = 0; i < k; i++) {
                outPutNode = stack.pop();
            }
        }

        return outPutNode;
    }

    public static void main(String[] args) {
        MyLinkedList linkedList = new MyLinkedList();
        linkedList.addEndNode( linkedList.new LinkedItem("A") );
        linkedList.addEndNode( linkedList.new LinkedItem("B") );
        linkedList.addEndNode( linkedList.new LinkedItem("C") );
        linkedList.addEndNode( linkedList.new LinkedItem("D") );
        linkedList.addEndNode( linkedList.new LinkedItem("E") );
        linkedList.addEndNode( linkedList.new LinkedItem("F") );
        System.out.println(linkedList);

        StackWay stackWay = new StackWay();
        System.out.println(stackWay.findBackNode(1,linkedList).value);
        System.out.println(stackWay.findBackNode(3,linkedList).value);
        System.out.println(stackWay.findBackNode(0,linkedList).value);
        System.out.println(stackWay.findBackNode(5,linkedList).value);
        System.out.println(stackWay.findBackNode(10,linkedList).value);
    }
}
