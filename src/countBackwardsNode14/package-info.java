package countBackwardsNode14;

/**
 * T-14 读取链表倒数第k个节点
 * DoublePointer --双指针，让两个指针保持 k - 1个节点的间距，前面指针指向尾节点时，后面指针则指向倒数第k个节点；
 * StackWay --借助栈：先让所有节点依次入栈，倒数k个节点就是第k次出栈的节点。
 *
 */