package Fibonacci.common;

public class IterationWay {

    //使用while循环
    public int getFunctionNumber(int n){
        int pre2 = 1, pre1 = 1;
        if(n==1||n==2){
            return 1;
        }
        while (n>2){
            int value = pre2 +  pre1;
            pre2 = pre1;
            pre1 = value;
            n--;
        }

        return pre1;
    }


    //使用for循环
    public int getFunctionNumber1(int n){
        int pre2 = 1, pre1 =  1,count = n;
        if(n==1||n==2){
            return 1;
        }

        for (int i = 2; i <=n; i++) {
            int value = pre2 +  pre1;
            pre2 = pre1;
            pre1 = value;
        }

        return pre1;
    }

    public static void main(String[] args) {
        IterationWay iterationWay = new IterationWay();
        int number1 = iterationWay.getFunctionNumber(12), number2 = iterationWay.getFunctionNumber(10);
        System.out.println("fn(12) = "+number1+"; fn(10) = "+number2);
    }

}
