package Fibonacci.common;

/**
 * 斐波那契数列--递归实现
 */
public class RecursionWay {

    public int getFunctionNumber(int n){
        if(n==1){
            return 1;
        }

        if(n==2){
            return 1;
        }


        return getFunctionNumber(n-1) + getFunctionNumber(n-2);
    }

    public static void main(String[] args) {
        RecursionWay recursionWay = new RecursionWay();
        int number1 = recursionWay.getFunctionNumber(12), number2 = recursionWay.getFunctionNumber(10);
        System.out.println("fn(12) = "+number1+"; fn(10) = "+number2);
    }

}
