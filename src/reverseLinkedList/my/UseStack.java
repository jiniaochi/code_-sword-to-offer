package reverseLinkedList.my;

import reverseLinkedList.dto.LinedListDto;
import reverseLinkedList.dto.LinkedItem;

import java.util.ArrayList;
import java.util.Stack;

/**
 * 使用栈的方式操作
 */
public class UseStack {

    public static void printReverseList(LinedListDto linedListDto){
        Stack<LinkedItem> stack = new Stack<>();
        LinkedItem node = linedListDto.getRoot();
        while (node.getNextItem()!=null){
            node = node.getNextItem();
            stack.push(node);
        }

        while (!stack.empty()){
            System.out.println(stack.pop());
        }
    }

}
