package reverseLinkedList.dto;

import java.util.LinkedList;
import java.util.List;

/**
 * ����
 */
public class LinedListDto {

    private LinkedItem root = new LinkedItem("root",null);

    public void addItem(LinkedItem item){
        if(item!=null) {
            LinkedItem node = root;
            while (node.getNextItem()!=null){
                node = node.getNextItem();
            }
            node.setNextItem(item);
        }
    }

    public LinkedItem getRoot() {
        return root;
    }

    public static void main(String[] args) {
        LinedListDto listDto = new LinedListDto();
        listDto.addItem(new LinkedItem("A",null));
        listDto.addItem(new LinkedItem("B",null));
        listDto.addItem(new LinkedItem("C",null));
        listDto.addItem(new LinkedItem("D",null));
        listDto.addItem(new LinkedItem("E",null));
        listDto.addItem(new LinkedItem("F",null));
        listDto.addItem(new LinkedItem("G",null));
        listDto.addItem(new LinkedItem("H",null));
        listDto.addItem(new LinkedItem("I",null));

        LinkedItem node = listDto.getRoot();
        while (node.getNextItem()!=null){
            node = node.getNextItem();
            System.out.println(node);
        }

    }
}
