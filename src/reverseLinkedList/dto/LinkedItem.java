package reverseLinkedList.dto;

/**
 * ����Ԫ��
 */
public class LinkedItem {
    private String data = null;
    private LinkedItem nextItem = null;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public LinkedItem getNextItem() {
        return nextItem;
    }

    public void setNextItem(LinkedItem nextItem) {
        this.nextItem = nextItem;
    }

    public LinkedItem(String data, LinkedItem nextItem) {
        this.data = data;
        this.nextItem = nextItem;
    }

    @Override
    public String toString() {
        return "LinkedItem{" +
                "data='" + data + '\'' +
                '}';
    }
}
