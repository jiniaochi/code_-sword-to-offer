package deleteNode12;

import deleteNode12.dto.MyLinkedList;

public class InputNode {

    //用后面节点得到值覆盖前面节点的值，再删除后面的节点 ，以达到相同的删除效果
    public void deleteNode(MyLinkedList.LinkedItem node,MyLinkedList list){
        if (node != null) {

            if (node.nextNode==null){//1.nextNode为null,代表删除节点为尾巴节点
                this.searchDelete(node,list);
            }else {//2.nextNode不为null
                this.recoverDelete(node);
            }
        }
    }

    //覆盖删除--O(1)
    private void recoverDelete(MyLinkedList.LinkedItem node){
        MyLinkedList.LinkedItem nextNode = node.nextNode;

        node.value = nextNode.value;
        node.nextNode = nextNode.nextNode;
        nextNode.nextNode =null;
    }

    //遍历查找删除--O(n)
    private boolean searchDelete(MyLinkedList.LinkedItem node, MyLinkedList list){
        MyLinkedList.LinkedItem preNode = list.root;
        while (preNode.nextNode != node){
            if (preNode==null){
                return false;//未找到节点
            }else {
                preNode = preNode.nextNode;
            }
        }

        //找到节点是，删除之
        preNode.nextNode = node.nextNode;
        node.nextNode = null;

        return true;
    }


    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.addEndNode( list.new LinkedItem("A"));
        list.addEndNode( list.new LinkedItem("B"));
        MyLinkedList.LinkedItem nodeC = list.new LinkedItem("C");
        list.addEndNode(nodeC);
        list.addEndNode( list.new LinkedItem("D"));
        list.addEndNode( list.new LinkedItem("E"));
        MyLinkedList.LinkedItem nodeF = list.new LinkedItem("F");
        list.addEndNode(nodeF);

        System.out.println(list);

        InputNode inputNode = new InputNode();
        inputNode.deleteNode(nodeC,list);
        System.out.println(list);
        inputNode.deleteNode(nodeF,list);
        System.out.println(list);
    }
}
