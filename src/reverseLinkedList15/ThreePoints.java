package reverseLinkedList15;

import reverseLinkedList15.dto.MyLinkedList;
import reverseLinkedList15.dto.MyLinkedList.LinkedItem;

public class ThreePoints {

    public void reverseLinkedList(MyLinkedList list){
        if (list!=null && list.length >0){
            LinkedItem p1 = list.root.nextNode, p2 = p1.nextNode==null?null:p1.nextNode ,p3 = p2==null||p2.nextNode==null?null:p2.nextNode;

            p1.nextNode = null;//尾节点指向null
            while (true){
                //1.确定三个指针的位置
                if (p3==null){
                    if (p2 != null) {
                        p2.nextNode = p1;
                        p1 = p2;
                        p2 = null;
                    }
                    break;

                }else {
                    p2.nextNode = p1;
                    p1 = p2;
                    p2 = p3;
                    p3 = p2.nextNode;
                }
            }

            list.root.nextNode = p1;
        }
    }

    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.addEndNode( list.new LinkedItem("A") );
        list.addEndNode( list.new LinkedItem("B") );
        list.addEndNode( list.new LinkedItem("C") );
        list.addEndNode( list.new LinkedItem("D") );
        list.addEndNode( list.new LinkedItem("E") );
        list.addEndNode( list.new LinkedItem("F") );
        list.addEndNode( list.new LinkedItem("G") );
        list.addEndNode( list.new LinkedItem("H") );
        System.out.println(list);

        ThreePoints threePoints = new ThreePoints();
        threePoints.reverseLinkedList(list);
        System.out.println(list);
    }
}
