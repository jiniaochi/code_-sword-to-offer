package adjustArr13;

import java.util.LinkedList;

public class DoubleQueue {

    public void swapInteger(int[] arr){
        LinkedList<Integer> oddQueue = new LinkedList<>();
        LinkedList<Integer> evenQueue = new LinkedList<>();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%2==0){
                evenQueue.offer(arr[i]);
            }else {
                oddQueue.offer(arr[i]);
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if (!oddQueue.isEmpty()){
                arr[i] = oddQueue.poll();
            }else {
                arr[i] = evenQueue.poll();
            }
        }

    }

    private void printArr(int[] arr){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < arr.length; i++) {
            builder.append(arr[i]+",");
        }
        builder.append("]");

        System.out.println(builder.toString());
    }

    public static void main(String[] args) {
        DoubleQueue doubleQueue = new DoubleQueue();
        int[] arr = {1, 3, 4, 6, 8, 11, 35, 66, 34, 45};
//        int[] arr = {2, 3, 8, 11};
        doubleQueue.printArr(arr);

        doubleQueue.swapInteger(arr);
        doubleQueue.printArr(arr);
    }

}
