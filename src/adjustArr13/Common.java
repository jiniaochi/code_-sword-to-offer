package adjustArr13;

public class Common {

    public void swapInteger(int[] arr){
        if (arr.length<=1){
            return;
        }

        for (int i = 0; i < arr.length; i++) {
            if (arr[i]%2==0){
                //1.发现偶数，将arr右边的元素左移；然后将arr[i]放在末位
                this.leftMove(i,arr);

            }
                //2.发现奇数，将arr右边的元素右移；然后将arr[i]放在0位。挪奇数或偶数二选一
//                this.rightMove(i,arr);

        }

    }

    //数组左端右移
    private void rightMove(int index,int[] arr){
        int value = arr[index];
        for (int i = index; i > 0; i--) {
            arr[i] = arr[i-1];
        }
        arr[0] = value;
    }

    //数组右端左移
    private  void  leftMove(int index,int[] arr){
        int value = arr[index];
        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }

        arr[arr.length - 1] = value;
    }

    private void printArr(int[] arr){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < arr.length; i++) {
            builder.append(arr[i]+",");
        }
        builder.append("]");

        System.out.println(builder.toString());
    }

    public static void main(String[] args) {
        Common common = new Common();
        int[] arr = {1, 3, 4, 6, 8, 11, 35, 66, 34, 45};
        common.printArr(arr);

        common.swapInteger(arr);
        common.printArr(arr);
    }
}
