package adjustArr13;

import java.util.Arrays;
import java.util.List;

public class DoublePointer {

    public void swapInteger(int[] arr){
        if (arr.length==0){
            return;
        }

        int oddIndex = 0, evenIndex = arr.length - 1;
        while (evenIndex>oddIndex){
            for (int i = oddIndex; i <= arr.length; i++) {
                oddIndex = i;
                if(arr[i]%2!=1){
                    break;
                }
            }

            for (int j = evenIndex; j >=0; j--) {
                evenIndex = j;
                if (arr[j]%2!=0){
                    break;
                }
            }

            if (evenIndex>oddIndex){
                this.swap(oddIndex,evenIndex,arr);
            }
        }

    }

    //������������
    private void swap(int index1,int index2,int[] arr){
        int swap = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = swap;
    }

    private void printArr(int[] arr){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < arr.length; i++) {
            builder.append(arr[i]+",");
        }
        builder.append("]");

        System.out.println(builder.toString());
    }

    public static void main(String[] args) {
        DoublePointer pointer = new DoublePointer();
        int[] arr = {1, 3, 4, 6, 8, 11, 35, 66, 34, 45};
//        int[] arr = {2, 3, 8, 11};
        pointer.printArr(arr);

        pointer.swapInteger(arr);
        pointer.printArr(arr);

    }

}
