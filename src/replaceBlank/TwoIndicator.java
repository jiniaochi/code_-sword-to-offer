package replaceBlank;

import java.util.Arrays;

/**
 * 官方方式：O(n)
 * 双指针移动法，且开辟的额外空间少
 */
public class TwoIndicator {

    public static int maxLength = 100;
    private char[] charsArr = new char[maxLength];
    private char replace = ' ';
    private int contentLength = 0;

    public TwoIndicator(String str){
        if(str==null || str.length()==0){
            System.err.println("不合法的主串");
            System.exit(0);
        }
        this.contentLength = str.length();
        char[] chars = str.toCharArray();
        for (int i = 0; i<this.contentLength  ; i++) {
            this.charsArr[i] = chars[i];
        }
        System.out.println(new String(charsArr));
    }

    public String replaceBlankByIndicator(){
        //1.先计算空格数目
        int blanckCount = 0;
        for (int i = 0; i < charsArr.length; i++) {
            if(charsArr[i]==' '){
                blanckCount++;
            }
        }

        /**2.双指针遍历：
         * pointOrigin 指向原字符数组的末尾，pointExtend指向扩展后的字符数组末尾；
         * pointOrigin 不断向前遍历：
         *  遇到非空字符，将此字符复制pointExtend处，且pointExtend减1；
         *  遇到空字符，跳过，将%20复制到pointExtend，pointExtend减3；
         *  当 pointExtend == pointOrigin 代表遍历结束
         */
        int pointOrigin = this.contentLength - 1, pointExtend = this.contentLength + blanckCount*2 - 1;
        while (pointExtend != pointOrigin){
            if(charsArr[pointOrigin] != ' '){
                charsArr[pointExtend] = charsArr[pointOrigin];
                pointExtend--;
            }else {
                charsArr[pointExtend] = '0';
                charsArr[pointExtend - 1] = '2';
                charsArr[pointExtend - 2] ='%';
                pointExtend -=3;
            }
            pointOrigin --;
        }

        return new String(charsArr);
    }
}
