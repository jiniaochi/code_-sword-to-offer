package replaceBlank;

import java.util.LinkedList;

/**、
 * 链表法: O(n)，但是会有额外得空间复杂度
 * 将元素放入链表中，组合
 */
public class LinkedListMethod {

    private char[] charsArr= null;
    private char replaceChar = ' ';

    public LinkedListMethod(String str){
        if(str==null || str.length()==0){
            System.err.println("不合法的主串");
            System.exit(0);
        }
        this.charsArr = str.toCharArray();
    }

    public String replaceBlankByLinkedList(){
        LinkedList<Character> linkedList = new LinkedList<>();
        for (int i = 0; i < charsArr.length; i++) {
            if(charsArr[i]== replaceChar){
                linkedList.add('%');
                linkedList.add('2');
                linkedList.add('0');
            }else {
                linkedList.add(charsArr[i]);
            }
        }
        StringBuilder builder = new StringBuilder();
        for(int j=0;j<linkedList.size();j++){
            builder.append(linkedList.get(j));
        }
        return builder.toString();
    }
}
