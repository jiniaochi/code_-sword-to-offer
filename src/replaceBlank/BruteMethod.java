package replaceBlank;

/**
 * 蛮力法，算法时间复杂度 O(n^2),主要时间消耗在数组向后移动元素
 */
public class BruteMethod {
    public static final int maxLength = 100;//字符数组最大长度1000
    private char[] charsArr = new char[maxLength];
    private char replaceChar = ' ';

    public BruteMethod(String str){
        if(str!=null && str.length()<=maxLength){
            for (int i=0;i<str.length();i++){
                this.charsArr[i] = str.charAt(i);
            }
        }else {
            System.exit(0);
            System.err.println("字符串最大长度超出限制，最大长度为--" + maxLength+";或者输入的字符串为无效字符串");
        }

    }

    public String bruteReplace(){
            // \u0000 为char基本类型的默认值
           for (int i=0;this.charsArr[i]!='\u0000';i++){
               if(this.charsArr[i]==this.replaceChar){//匹配上，则先将后续元素后移动，再填入元素
                  for (int j = charsArr.length -1;j>i;j--){//右移动2格
                      if(charsArr[j]!='\u0000'){
                          charsArr[j+2] = charsArr[j];
                      }
                  }
                   charsArr[i] = '%';
                   charsArr[i+1] = '2';
                   charsArr[i+2] = '0';
               }
           }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < charsArr.length; i++) {
            if(charsArr[i]=='\u0000'){
                break;
            }else {
                builder.append(charsArr[i]);
            }
        }
        return builder.toString();
    }
}
