package rebuildTree.dto;

/**
 * ���Ľڵ�
 */
public class TreeNode {

    private String data = null;
    public TreeNode leftChild = null;
    public TreeNode rightChild = null;

    public TreeNode(String data) {
        this.data = data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public TreeNode getLeftChild() {
        return leftChild;
    }

    public void setLeftChild(TreeNode leftChild) {
        this.leftChild = leftChild;
    }

    public TreeNode getRightChild() {
        return rightChild;
    }

    public void setRightChild(TreeNode rightChild) {
        this.rightChild = rightChild;
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data='" + data + '\'' +
                '}';
    }
}
