package rebuildTree.Q1;

import rebuildTree.dto.TreeNode;

import java.util.Arrays;
import java.util.Optional;

/**
 * 根据先序遍历和中序遍历确定一棵树
 * tip: 当巨大、复杂的函数/方法发现自己写得没把握时，可以整理思路，将复杂得函数拆分为几个相对简单函数。拆分法
 */
public class RebuildTree1 {

    public RebuildTree1(){

    }

    public void rebuild(String[] firstArr,String[] middleArr){
        TreeNode  root = addNode(firstArr,middleArr,0,firstArr.length - 1,0,middleArr.length -1 );
        System.out.println("over");
    }

    private TreeNode addNode(String[] firstArr,String[] middleArr,int firstStart,int firstEnd,int middleFirst,int middleEnd){
        //1.递归出口
        if(firstStart>firstEnd||middleFirst>middleEnd){
            return null;
        }

        TreeNode node = new TreeNode(firstArr[firstStart]);
        //找到根节点
        int index = -1;
        for (int i = middleFirst; i <= middleEnd; i++) {
            if(middleArr[i].equals(firstArr[firstStart]) ){
                index = i;
                break;
            }
        }
        if(index==-1){
            return null;
        }

        //2.递归递归关系
        node.leftChild = addNode(firstArr,middleArr,firstStart + 1,firstStart+index-middleFirst,middleFirst,index-1);//构建左子树
        node.rightChild = addNode(firstArr,middleArr,firstStart+index-middleFirst+1,firstEnd,index+1,middleEnd);//构建右子树
        return node;
    }

    public static void main(String[] args) {
        String[] firstArr = new String[]{"1","2","4","7","3","5","6","8"};
        String[] middleArr = new String[]{"4","7","2","1","5","3","8","6"};
        RebuildTree1 tree1 = new RebuildTree1();
        tree1.rebuild(firstArr,middleArr);
    }
}
