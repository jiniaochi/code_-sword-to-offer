package rebuildTree.Q2;

import rebuildTree.dto.TreeNode;

/**
 * 根据后序遍历和中序遍历确定一棵树
 * tip: 当巨大、复杂的函数/方法发现自己写得没把握时，可以整理思路，将复杂得函数拆分为几个相对简单函数。拆分法
 */
public class RebuildTree2 {

    public RebuildTree2(String[] middleArr,String[] lastArr){
        if(middleArr!=null&&lastArr!=null){
            if(middleArr.length==lastArr.length&&lastArr.length>0){
                TreeNode root = addNode(middleArr,lastArr,0,middleArr.length-1,0,lastArr.length-1);
                System.out.println(root);
            }
        }
    }

    private TreeNode addNode(String[] middleArr,String[] lastArr,int middleStart,int middleEnd,int lastStart,int lastEnd){
        //递归出口
        if(middleStart>middleEnd||lastStart>lastEnd){
            return null;
        }

        //递推关系
        TreeNode node = new TreeNode(lastArr[lastEnd]);
        int index = -1;
        for (int i = middleStart; i <= middleEnd; i++) {
            if(lastArr[lastEnd].equals(middleArr[i])){
                index = i;
                break;
            }
        }

        if(index==-1){
            return null;
        }

        node.leftChild = addNode(middleArr,lastArr,middleStart,index-1,lastStart,index-middleStart + lastStart - 1);//左子树
        node.rightChild = addNode(middleArr,lastArr,index+1,middleEnd,index-middleStart+lastStart,lastEnd-1);//右子树

        return node;
    }

    /**
     * 中序遍历:2 4 1 5 7 3 6
     * 后序遍历:4 2 7 5 6 3 1
     * @param args
     */
    public static void main(String[] args) {
        String[] middleArr = new String[]{"2","4","1","5","7","3","6"};
        String[] lastArr = new String[]{"4","2","7","5","6","3","1"};
        RebuildTree2 tree2 = new RebuildTree2(middleArr,lastArr);
    }

}
