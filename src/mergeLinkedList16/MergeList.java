package mergeLinkedList16;

import mergeLinkedList16.dto.MyLinkedList;
import mergeLinkedList16.dto.MyLinkedList.LinkedItem;

public class MergeList {

    public MyLinkedList mergeList(MyLinkedList list1,MyLinkedList list2){
        if (!isValidList(list1)&&!isValidList(list2)){
            return null;
        }else if(isValidList(list1)&&!isValidList(list2)){
            return list1;
        }else if (!isValidList(list1)&&isValidList(list2)){
            return list2;
        }else {
            MyLinkedList mergeList = new MyLinkedList();
            LinkedItem p1 = list1.root.nextNode,p2 = list2.root.nextNode,p3 = mergeList.root;
            //1.两个链表都未读取完
            while (p1!=null && p2!=null){
                if (Integer.parseInt(p1.value)<Integer.parseInt(p2.value)){
                    p3.nextNode = mergeList.new LinkedItem(p1.value);
                    p1 = p1.nextNode;
                }else {
                    p3.nextNode = mergeList.new LinkedItem(p2.value);
                    p2 = p2.nextNode;
                }
                p3 = p3.nextNode;
            }

            if (p1==null && p2!=null){//2.1 链表1读取完而链表2未读取完
                while (p2!=null){
                    p3.nextNode = mergeList.new LinkedItem(p2.value);
                    p2 = p2.nextNode;
                    p3 = p3.nextNode;
                }
            }else if(p1!=null && p2==null){//2.2 链表2读取完而链表1未读取完
                while (p1!=null){
                    p3.nextNode = mergeList.new LinkedItem(p1.value);
                    p1 = p1.nextNode;
                    p3 = p3.nextNode;
                }
            }else {//2.3 两表均读取完毕

            }
            return mergeList;
        }

    }

    private boolean isValidList(MyLinkedList list){
        return list !=null && list.length>0;
    }

    public static void main(String[] args) {
        MyLinkedList list1 = new MyLinkedList();
        list1.addEndNode( list1.new LinkedItem("1") );
        list1.addEndNode( list1.new LinkedItem("3") );
        list1.addEndNode( list1.new LinkedItem("5") );
        list1.addEndNode( list1.new LinkedItem("7") );
        list1.addEndNode( list1.new LinkedItem("8") );
        list1.addEndNode( list1.new LinkedItem("12") );
        System.out.println(list1);

        MyLinkedList list2 = new MyLinkedList();
        list2.addEndNode( list2.new LinkedItem("2") );
        list2.addEndNode( list2.new LinkedItem("4") );
        list2.addEndNode( list2.new LinkedItem("6") );
        list2.addEndNode( list2.new LinkedItem("8") );
        list2.addEndNode( list2.new LinkedItem("11") );
        list2.addEndNode( list2.new LinkedItem("15") );
        System.out.println(list2);

        System.out.println(new MergeList().mergeList(list1,list2));
    }
}
