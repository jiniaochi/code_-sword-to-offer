package isSubTree17.dto;

public class MyTree {

    public class MyTreeNode{
        public String value = "";
        public MyTreeNode leftChild = null;
        public MyTreeNode rightChild = null;

        public  MyTreeNode(){}

        public MyTreeNode(String value) {
            this.value = value;
        }
    }

    //��¼���ڵ�
    private MyTreeNode root = null;

    public MyTreeNode getRoot() {
        return root;
    }

    public void setRoot(MyTreeNode root) {
        this.root = root;
    }
}
