package isSubTree17;

import isSubTree17.dto.MyTree;
import isSubTree17.dto.MyTree.MyTreeNode;

import java.util.LinkedList;

public class IsSubTree {

    //tree1 是主树
    public boolean isSub(MyTree tree1,MyTree tree2){
//        return this.judgeNode(tree1.getRoot(),tree2.getRoot());

        LinkedList<MyTreeNode> queue = new LinkedList<>();
        MyTreeNode root2 = tree2.getRoot();
        queue.offer(tree1.getRoot());
        while (!queue.isEmpty()){
            MyTreeNode node1 = queue.poll();
            if (node1==null){
                continue;
            }

            if(judgeNode(node1,root2)){
                return true;
            }else {
                queue.offer(node1.leftChild);
                queue.offer(node1.rightChild);
            }
        }

        return false;
    }

    //node1 主树节点
    private boolean judgeNode(MyTreeNode node1, MyTreeNode node2){
        //递归出口
        if(node1 == null && node2==null){
            return true;
        }else if(node1 != null && node2 != null){
            //递推关系
            if (node1.value.equals(node2.value)){
                return judgeNode(node1.leftChild,node2.leftChild) && judgeNode(node1.rightChild,node2.rightChild);
            }else {
                return false;
            }
        }else if (node1!=null&&node2==null){
            return true;
        }else {
            return false;
        }
    }

    public static void main(String[] args) {
        MyTree myTree1 = new MyTree(),myTree2 = new MyTree();
        //构建tree1
        MyTreeNode nodeA1_1 = myTree1.new MyTreeNode("8");
        MyTreeNode nodeA2_1 = myTree1.new MyTreeNode("8");
        MyTreeNode nodeA2_2 = myTree1.new MyTreeNode("7");
        MyTreeNode nodeA3_1 = myTree1.new MyTreeNode("9");
        MyTreeNode nodeA3_2 = myTree1.new MyTreeNode("2");
        MyTreeNode nodeA4_1 = myTree1.new MyTreeNode("4");
        MyTreeNode nodeA4_2 = myTree1.new MyTreeNode("7");
        myTree1.setRoot(nodeA1_1);
        nodeA1_1.leftChild = nodeA2_1;
        nodeA1_1.rightChild = nodeA2_2;
        nodeA2_1.leftChild = nodeA3_1;
        nodeA2_1.rightChild = nodeA3_2;
        nodeA3_2.leftChild = nodeA4_1;
        nodeA3_2.rightChild = nodeA4_2;

        //构建tree2
        MyTreeNode nodeB1_1 = myTree2.new MyTreeNode("8");
        MyTreeNode nodeB2_1 = myTree2.new MyTreeNode("9");
        MyTreeNode nodeB2_2 = myTree2.new MyTreeNode("2");
        myTree2.setRoot(nodeB1_1);
        nodeB1_1.leftChild = nodeB2_1;
        nodeB1_1.rightChild = nodeB2_2;

        System.out.println(new IsSubTree().isSub(myTree1,myTree2));
    }
}
