package reverseArrMin;

public class FindMinNumber {

    private final int[] arr;

    public FindMinNumber(int[] arr){
        this.arr = arr;
    }

    /**
     * 找到反转数组（升序）最小值
     * @return 最小值的索引，不存在或输入不合法返回-1
     */
    public int findMinNum(){
        return dichotomy(this.arr);
    }

    /**
     * 二分法查找最小元素，使用到有序旋转数组的特性
     * --因为 (startIndex+endIndex)/2 整除的特性，最后肯定是endInddex、middleIndex、startIndex三者指向同一元素，
     * --或者middleIndex和startIndex指向同一元素，而endIndex指向+1的位置
     */
    public int dichotomy(int[] arr){
        if(arr == null){
            return -1;
        }

        int startIndex = 0, endIndex = arr.length - 1, middleIndex = (startIndex + endIndex)/2;
        while (endIndex > startIndex&&middleIndex!=startIndex){

            if(arr[endIndex]< arr[middleIndex]){//最小值在中点右边
                startIndex = middleIndex;
            }else if (arr[startIndex] > arr[middleIndex]){//最小值在中点左边
                endIndex = middleIndex;
            }else{
                //二者相等，特殊情况
                return brudeFind(arr,startIndex,endIndex);
            }

            middleIndex = (startIndex + endIndex)/2;
        }


        return endIndex;
    }

    //蛮力法查找最小元素，未使用到有序旋转数组的特性
    public int brudeFind(int[] arr,int startIndex,int endIndex){
        if (arr == null) {
            return -1;
        }
        int index = startIndex;
        for (int i = startIndex; i <= endIndex; i++) {
            if( arr[i] < arr[index]){
                index = i;
            }
        }
        return index;
    }


    public static void main(String[] args) {
//        int[] arr = {4, 5, 6, 7, 1, 2, 3};
//        FindMinNumber findMinNumber = new FindMinNumber(arr);
//        System.out.println(findMinNumber.findMinNum());

//        int[] arr2 = {6, 7, 1, 2, 3, 4, 5};
//        FindMinNumber findMinNumber2 = new FindMinNumber(arr2);
//        System.out.println(findMinNumber2.findMinNum());

        int[] arr3 = {1,0,1,1};
        FindMinNumber findMinNumber3 = new FindMinNumber(arr3);
        System.out.println(findMinNumber3.findMinNum());
    }
}
