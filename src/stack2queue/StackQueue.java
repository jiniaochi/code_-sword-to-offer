package stack2queue;

import java.util.Stack;

public class StackQueue {

    private Stack<String> stack1 = new Stack<>();//用于存储元素
    private Stack<String> stack2 = new Stack<>();//用于队列弹出元素，注意：只有stack2为空后，才能再次入stack2

    public void addElement(String element){
        stack1.push(element);
    }

    public String removeElement(){
        if(stack2.empty()){//栈1空，栈1全部进入栈2；否则直接返回栈2栈顶元素
            while (!stack1.empty()){
                stack2.push(stack1.pop());
            }
        }
        return  stack2.pop();
    }
}
