package NumberOf1;
/**
 * T8-求一二进制数字中1的个数，存在以下四种方式：
 * 1.common--一般思路
 * 2.rightMove--右移
 * 3.leftMove--左移
 * 4.subtraction--减1后与原数做与操作
 */